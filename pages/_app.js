import App, { Container } from 'next/app'
import Head from 'next/head';

import '../src/style/index.css'

export default class MyApp extends App {
    static async getInitialProps({ Component, ctx }) {
        let pageProps = {}
        if(Component.getInitialProps) {
            pageProps = await Component.getInitialProps(ctx);
        }
        return { pageProps }
    }
    render() {
        const { pageProps, Component } = this.props;
        return (
            <Container>
                <Head>
                    <title>Hello CSS</title>
                </Head>
                <Component {...pageProps} />
            </Container>
        )
    }
}