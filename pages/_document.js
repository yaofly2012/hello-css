import Document, { Head, Main, NextScript } from 'next/document'

export default class CCardDocument extends Document {

  	render() {
		
		return (
			<html>
				<Head>
					<meta name="viewport" content="width=device-width,initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
					<meta content="telephone=no" name="format-detection" />
					<meta name="apple-mobile-web-app-capable" content="yes" />
					<meta charSet="utf-8" />
                </Head>
				<body className='search-body'>
					<Main />
					<script src=" http://webresource.fws.qa.nt.ctripcorp.com/code/lizard/2.2/web/lite.seed.js?v=sdfasdfas"></script>
					<NextScript />					
				</body>
			</html>
		)
  	}
}
