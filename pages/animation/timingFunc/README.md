

```
steps(stepCount, position)
```
1. stepCount
步数
2. position（start, end）
计算值发生变化的位置（步前，步后）
- start（步前）
即还没开始就瞬间移位了（起步早）

- end（步后）
即等当前步时间片后再计算值（起步晚）

# 参考
1. [How to Use steps() in CSS Animations](https://designmodo.com/steps-css-animations/)
2. [CSS3 animation属性中的steps功能符深入介绍](https://www.zhangxinxu.com/wordpress/2018/06/css3-animation-steps-step-start-end/)