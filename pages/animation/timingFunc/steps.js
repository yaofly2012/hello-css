export default function Index() {
    return (
        <div>
            <h2>Timing function - steps</h2>
            <div className="progress">
                <div className="indicator"></div>
            </div>
            <div className="progress">
                <div className="indicator indicator--end"></div>
            </div>
            <div className="progress">
                <div className="indicator indicator--one"></div>
            </div>
            <style jsx>{`
                .progress {                 
                    background: #e5e5e5;
                    margin-bottom: 15px;
                }

                .indicator {
                    position: relative;
                    width: 0px;         
                    height: 30px;   
                    background: #C28142;
                    animation-name: group;
                    animation-duration: 4s;
                    animation-iteration-count: infinite;
                    animation-timing-function: steps(4, start);
                }

                .indicator--end {
                    animation-timing-function: steps(4, end);
                }

                .indicator--one {
                    animation: blink 1s infinite steps(1, start);
                }
                
                @keyframes group {
                    0% {
                        width: 0px;
                    }

                    100% {
                        width: 100%;
                    }
                }

                @keyframes blink {
                    from, to {
                        width: 0px;
                    }

                    50% {
                        width: 100%;
                    }
                }
            `}</style>
        </div>
    )
}