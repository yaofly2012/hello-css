// flex-grow normal
function demo1() {
    return (
        <>
            <div className="flex">
                <div className="item" style={{flexBasis: 100, flexGrow: 1, marginRight: 10}}>One</div>
                <div className="item" style={{flexBasis: 150, flexGrow: 2, }}>Two</div>
            </div>
            <style jsx>{`
                .flex {
                    display: flex;
                    width: 600px;
                    outline: 1px dashed red;
                }
                .item {
                    padding: 10px;
                    border: 10px solid #666666;
                }
            `}</style>
        </>
    )
}

// flex-grow box-sizing=border
function demo2() {
    return (
        <>
            <div className="flex">
                <div className="item" style={{flexBasis: 100, flexGrow: 1, marginRight: 10}}>One</div>
                <div className="item" style={{flexBasis: 150, flexGrow: 2, }}>Two</div>
            </div>
            <style jsx>{`
                .flex {
                    display: flex;
                    width: 600px;
                    outline: 1px dashed red;
                }
                .item {
                    padding: 10px;
                    border: 10px solid #666666;
                    box-sizing: border-box;
                }
            `}</style>
        </>
    )
}

// flex-grow SUM(flex-grow) < 1
function demo3() {
    return (
        <>
            <div className="flex">
                <div className="item" style={{flexBasis: 100, flexGrow: 0.2, marginRight: 10}}>One</div>
                <div className="item" style={{flexBasis: 150, flexGrow: 0.3, }}>Two</div>
            </div>
            <style jsx>{`
                .flex {
                    display: flex;
                    width: 600px;
                    outline: 1px dashed red;
                }
                .item {
                    padding: 10px;
                    border: 10px solid #666666;
                    box-sizing: border-box;
                }
            `}</style>
        </>
    )
}

// flex-grow  maxWidth 
/**
 * maxWidh规则第一，如果宽度大于maxWidth, 则取值maxWidth. 
 * 剩下的重新计算
 */
function demo4() {
    return (
        <>
            <div className="flex">
                <div className="item" style={{flexBasis: 100, flexGrow: 1, marginRight: 10, maxWidth: 150}}>One</div>
                <div className="item" style={{flexBasis: 150, flexGrow: 2 }}>Two</div>
                <div className="item" style={{flexBasis: 100, flexGrow: 3 }}>Three</div>
            </div>
            <style jsx>{`
                .flex {
                    display: flex;
                    width: 800px;
                    outline: 1px dashed red;
                }
                .item {
                    padding: 10px;
                    border: 10px solid #666666;
                    box-sizing: border-box;
                }
            `}</style>
        </>
    )
}


// shrink
function demo5() {
    return (
        <>
            <div className="flex">
                <div className="item" style={{flexBasis: 100, flexGrow: 1, marginRight: 10}}>One</div>
                <div className="item" style={{flexBasis: 150, flexGrow: 2, flexShrink: 2 }}>Two</div>
            </div>
            <style jsx>{`
                .flex {
                    display: flex;
                    width: 300px;
                    outline: 1px dashed red;
                }
                .item {
                    padding: 10px;
                    border: 10px solid #666666;
                }
            `}</style>
        </>
    )
}

// shrink border-box
function demo6() {
    return (
        <>
            <div className="flex">
                <div className="item" style={{flexBasis: 100, flexGrow: 1, marginRight: 10}}>One</div>
                <div className="item" style={{flexBasis: 150, flexGrow: 2, flexShrink: 2 }}>Two</div>
            </div>
            <style jsx>{`
                .flex {
                    display: flex;
                    width: 200px;
                    outline: 1px dashed red;
                }
                .item {
                    padding: 10px;
                    border: 10px solid #666666;
                    box-sizing: border-box;
                }
            `}</style>
        </>
    )
}

// shrink min
function demo7() {
    return (
        <>
            <div className="flex">
                <div className="item" style={{flexBasis: 100, flexShrink: 2, marginRight: 10, minWidth: 60}}>One</div>
                <div className="item" style={{flexBasis: 150, flexShrink: 2 }}>Two</div>
                <div className="item" style={{flexBasis: 100, flexShrink: 1 }}>Three</div>
            </div>
            <style jsx>{`
                .flex {
                    display: flex;
                    width: 300px;
                    outline: 1px dashed red;
                }
                .item {
                    padding: 10px;
                    border: 10px solid #666666;
                }
            `}</style>
        </>
    )
}

// fhrink SUM_flex_shrink < 1
function demo8() {
    return (
        <>
            <div className="flex">
                <div className="item" style={{flexBasis: 100, flexShrink: 0.2, marginRight: 10}}>One</div>
                <div className="item" style={{flexBasis: 150, flexShrink: 0.3 }}>Two</div>
            </div>
            <style jsx>{`
                .flex {
                    display: flex;
                    width: 300px;
                    outline: 1px dashed red;
                }
                .item {
                    padding: 10px;
                    border: 10px solid #666666;
                }
            `}</style>
        </>
    )
}

//
function demo9() {
    return (
        <>
            <div className="flex">
                <div className="item">One</div>
                <div className="item" style={{width: 100}}>Two</div>
                <div className="item">Three</div>
            </div>
            <style jsx>{`
                .flex {
                    display: flex;
                    width: 300px;
                    outline: 1px dashed red;
                    flex-flow: row wrap;
                }
                .item {
                    width: 80px;
                    padding: 10px;
                    border: 10px solid #666666;
                    {/* flex: 1; */}
                }
            `}</style>
        </>
    )
}
export default () => {
    return demo9()
}

