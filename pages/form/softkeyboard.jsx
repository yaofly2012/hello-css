import { useLayoutEffect } from 'react'

export default function Index() {

    useLayoutEffect(() => {
        window.LizardLite.HybridReady(() => {
            debugger

        })
    }, [])

    return (
        <>
            <div className="page">
                <h1 className="page-header">Form & SoftKeyboard</h1>
                <div className="page-body">
                    <ul className="page-form">
                        <li>
                            <input placeholder="Name" />
                        </li>
                        <li>
                            <input placeholder="LastName" />
                        </li>
                        <li>
                            <input placeholder="FirstName" />
                        </li>
                    </ul>
                </div>
                <button className="page-footer">Submit</button>
            </div>
            <style jsx>{`               
                .page {
                    width: 100vw;
                    height: 100vh;
                    display: flex;
                    flex-flow: column nowrap;
                    overflow: hidden;
                }
                
                .page-header {
                    padding: 15px 0;
                    flex: 0 0 auto;
                    background-color: yellowgreen;
                }

                .page-body {
                    flex: 1 1 0;
                    margin: 30px 15px;
                    overflow: scroll;
                }
                .page-footer {
                    flex: 0 0 auto;
                }

                .page-form {
                    height: 150vh;
                }

                input {
                    width: 100%;
                }

                button, input {
                    height: 44px;
                }


            `}</style>
        </>
    )
}