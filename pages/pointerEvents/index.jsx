

export default function Index() {
    
    function handlePointerEvent(e) {      
        console.group(e.type)     
        console.log(`e.target.id=${e.target.id}`)   
        console.log(`e.currentTarget.id=${e.currentTarget.id}`)           
        console.groupEnd(e.type)
    }

    return (
        <div  style={{margin: '30px'}}>
            <h1>pointer-events</h1>
            <div style={{position: 'relative'}}>            
                <div id="outer" className="box" 
                    onTouchStart={handlePointerEvent}
                    onTouchMove={handlePointerEvent}
                    onTouchEnd={handlePointerEvent}
                    onTouchCancel={handlePointerEvent}
                    onClick={handlePointerEvent}>                        
                    <p>Backend</p>                    
                </div> 
                <div id="inner" className="box box--small" 
                        onTouchStart={handlePointerEvent}
                        onTouchMove={handlePointerEvent}
                        onTouchEnd={handlePointerEvent}
                        onTouchCancel={handlePointerEvent}
                        onClick={handlePointerEvent}             
                    >Front</div>                               
            </div>
            <div className="scroll-wrap">
                <div className="scroll-content">
                    <ul>
                        <li>01月</li>
                        <li>02月</li>
                        <li>03月</li>
                        <li>04月</li>
                        <li>05月</li>
                        <li>06月</li>
                        <li>07月</li>
                        <li>08月</li>
                        <li>09月</li>
                        <li>10月</li>
                        <li>11月</li>
                        <li>12月</li>
                    </ul>
                </div>
            </div>
            <style jsx>{`
                .scroll-wrap {
                    height: 100px;
                    width: 200px;
                    position: relative;
                }
                .scroll-wrap::before {
                    position: absolute;
                    content: "";
                    top: 0;
                    height: 50px;
                    width: 100%;
                    left: 0;
                    pointer-events: none;
                    background: linear-gradient(to bottom,rgb(104, 188, 226), rgba(255, 255, 255, 0.4))
                }

                .scroll-wrap::after {
                    position: absolute;
                    content: "";
                    bottom: 0;
                    height: 50px;
                    width: 100%;
                    left: 0;
                    pointer-events: none;
                    background: linear-gradient(to top,rgb(104, 188, 226),rgba(255, 255, 255, 0.4))
                }
                .scroll-content {
                    height: 100%;                    
                    overflow-y: scroll;
                    text-align: center;
                }
                ul {
                    padding: 20px 0;
                }
                .box {
                    width: 50vw;
                    height: 40vw;
                    background: yellowgreen;
                }

                .box--small {
                    width: 40vw;
                    height: 30vw;
                    background: aquamarine;
                    pointer-events: none;
                    margin: 0 auto;
                    position: absolute;
                    top: 30px;
                    left: 0;
                }
            `}</style>
        </div>
    )
}