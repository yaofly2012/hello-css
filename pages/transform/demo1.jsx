export default () => (<div>
    <div className="box tranlateP">
        Block box        
    </div>
    <span className="box tranlateP">Inline box</span>
    <br/>
    <input className="tranlateP"/>
    <br/>
    <img  className="box tranlateP" src="https://interactive-examples.mdn.mozilla.net/media/examples/firefox-logo.svg"/>
    <style jsx>{`
        .box {
            width: 100px;
            height: 100px;
            background: #ccc;
        }
        .tranlateP {
            transform: translate(120px, 0);
        }
    `}</style>
</div>)

/**
 * 1. Only transformable elements can be transformed. That is, all elements whose layout is governed by the CSS box model except for: non-replaced inline boxes, table-column boxes, and table-column-group boxes.
 */